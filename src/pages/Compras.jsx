import { useState, useEffect } from 'react'
import generarCompra from '../services/generarCompra'
import getCompras from '../services/getCompras'

const datosIniciales = {
  prov_id: '',
  farmaco_id: '',
  cantidad: ''
}

export default function Compras() {
  const [data, setData] = useState([])
  const [reload, setReload] = useState(false)

  const [form, setForm] = useState(datosIniciales)
  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    })
  }
  const handleSubmit = (e) => {
    e.preventDefault()

    if (!form.prov_id || !form.farmaco_id || !form.cantidad) {
      alert('¡Llena los datos correctamente!')
      return
    }else{
      const compra = async () =>{
        const res = await generarCompra(form)
        alert(res.notificacion)
        setReload(!reload)
        setForm(datosIniciales)
      }
      compra()
    }
  }

  useEffect(function(){
    getCompras().then(compras => setData(compras))
  }, [reload])

  return (
    <>
      <h1><i>Compras</i></h1>
      <div>
          <h3>Crear una compra</h3>
          <form onSubmit={handleSubmit}>

              <label htmlFor="id">Id del proveedor: </label> 
              <input type="number" name="prov_id" id="id"
              onChange={handleChange} value={form.prov_id}/>

              <label htmlFor="farm">Id del fármaco: </label>
              <input type="number" name="farmaco_id" id="farm"
              onChange={handleChange} value={form.farmaco_id}/> <br /> <br />

              <label htmlFor="cantidad">Cantidad: </label>
              <input type="number" name="cantidad" id="cantidad"
              onChange={handleChange} value={form.cantidad}/>

              <input type="submit" value="Crear compra"/>
          </form>
      </div>
      <div>
        <h3>Movimientos de compras</h3>
        <table id="customers">
        <tr>
          <th>Id Fármaco</th>
          <th>Fármaco</th>
          <th>Precio</th>
          <th>Costo total</th>
          <th>Stock</th>
          <th>Fecha</th>
          <th>Id Proveedor</th>
          <th>Proveedor</th>
        </tr>
        {
          data.map((compra) => {
            return(
              <tr key={compra.prov_nombre}>
                <td>{compra.farmaco_id}</td>
                <td>{compra.farmaco_nombre}</td>
                <td>{compra.farmaco_precio}</td>
                <td>{compra.adqui_costo}</td>
                <td>{compra.farmaco_stock}</td>
                <td>{compra.adqui_fecha.substring(0, 10)}</td>
                <td>{compra.prov_id}</td>
                <td>{compra.prov_nombre}</td>
              </tr>
            )
          })
        }
        </table>
      </div>
    </>
  )
}
