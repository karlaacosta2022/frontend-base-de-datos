import { useState, useEffect } from 'react'
import updateCliente from '../services/clientes/updateClientes'
import deleteCliente from '../services/clientes/deleteClientes'
import readClientes from '../services/clientes/readClientes'

const datosIniciales = {
  cli_id: '',
  cli_cedula: '',
  cli_nombres: '',
  cli_apellidos: '',
  cli_direccion: '',
  cli_telefono: '',
  cli_email: '',
  cli_fechanacimiento: '',
  cli_prescripcion_: ''
}

function Clientes() {
  const [data, setData] = useState([])
  const [reload, setReload] = useState(false)

  const [form, setForm] = useState(datosIniciales)
  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    })
  }
  const handleSubmit = (e) => {
    e.preventDefault()

    if (!form.cli_id|| !form.cli_cedula|| !form.cli_nombres|| !form.cli_apellidos|| !form.cli_direccion||
      !form.cli_telefono|| !form.cli_email|| !form.cli_fechanacimiento || !form.cli_prescripcion_ ) {
      alert('Datos incompletos')
      return
    }else{
      const actualizacion = async () => {
        const { cli_id, cli_cedula, cli_nombres, cli_apellidos, cli_direccion, cli_telefono,
          cli_email, cli_fechanacimiento, cli_prescripcion_ } = form
        const res = await updateCliente(cli_id, {cli_id, cli_cedula, cli_nombres, cli_apellidos, cli_direccion, cli_telefono,
          cli_email, cli_fechanacimiento, cli_prescripcion_})

        alert(res.notificacion)
        setReload(!reload)
        setForm(datosIniciales)
      }
      actualizacion()
    }
  }

  useEffect(function(){
    readClientes().then(v => setData(v))
  }, [reload])

  return (
    <>
      <div>
        <h1><i>Clientes</i></h1>
        <div>
          <h3>Crea o actualiza un cliente</h3>
          <form onSubmit={handleSubmit}>
            <div>
              <label htmlFor="id">Id del cliente: </label> 
              <input type="number" name="cli_id" id="id"
              onChange={handleChange} value={form.cli_id} className='right'/>

              <label htmlFor="id2">Cédula: </label> 
              <input type="number" name="cli_cedula" id="id2"
              onChange={handleChange} value={form.cli_cedula} className='right'/>

              <label htmlFor="id3">Nombres: </label> 
              <input type="text" name="cli_nombres" id="id3"
              onChange={handleChange} value={form.cli_nombres} className='right'/> <br /> <br />

              <label htmlFor="id4">Apellidos: </label> 
              <input type="text" name="cli_apellidos" id="id4"
              onChange={handleChange} value={form.cli_apellidos} className='right'/>

              <label htmlFor="cantidad">Dirección: </label>
              <input type="text" name="cli_direccion" id="cantidad"
              onChange={handleChange} value={form.cli_direccion} className='right'/>

              <label htmlFor="cantidad2">Teléfono: </label>
              <input type="number" name="cli_telefono" id="cantidad2"
              onChange={handleChange} value={form.cli_telefono} className='right'/> <br /> <br />

              <label htmlFor="cantidad3">Email: </label>
              <input type="text" name="cli_email" id="cantidad3"
              onChange={handleChange} value={form.cli_email} className='right'/>

              <label htmlFor="cantidad4">Fecha nacimiento: </label>
              <input type="date" name="cli_fechanacimiento" id="cantidad4"
              onChange={handleChange} value={form.cli_fechanacimiento} className='right'/>

              <label htmlFor="id5">Prescripción: </label> 
              <input type="text" name="cli_prescripcion_" id="id5"
              onChange={handleChange} value={form.cli_prescripcion_} className='right'/> <br /> <br />

              <input type="submit" value="Enviar"/>
            </div>
          </form>
        </div>

        <div>
          <h3>Listado de Clientes</h3>
          <table id="customers">
          <tr>
            <th>Id cliente</th>
            <th>Cédula</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Dirección</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Fecha nacimiento</th>
            <th>Prescripción</th>
            <th colSpan={2}>Acciones</th>
          </tr>
          {
            data.map((c) => {
              return(
                <tr key={c.cli_id}>
                  <td>{c.cli_id}</td>
                  <td>{c.cli_cedula}</td>
                  <td>{c.cli_nombres}</td>
                  <td>{c.cli_apellidos}</td>
                  <td>{c.cli_direccion}</td>
                  <td>{c.cli_telefono}</td>
                  <td>{c.cli_email}</td>
                  <td>{c.cli_fechanacimiento.substring(0, 10)}</td>
                  <td>{c.cli_prescripcion_.toString()}</td>
                  <td>
                      <button
                        onClick={() => {
                          c.cli_fechanacimiento = c.cli_fechanacimiento.slice(0, 10)
                          setForm(c)
                        }}
                      >Editar</button>
                    </td>
                    <td>
                      <button
                        onClick={ async () => {
                          await deleteCliente(c.cli_id)
                          setReload(!reload)
                        }}
                      >Eliminar</button>
                    </td>
                </tr>
              )
            })
          }
          </table>
        </div>
        
      </div>
    </>
  )
}

export default Clientes
