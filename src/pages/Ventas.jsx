import { useState, useEffect } from 'react'
import generarVenta from '../services/generarVenta'
import getVentas from '../services/getVentas'

const datosIniciales = {
  ven_id: '',
  cli_id: '',
  farmaco_id: '',
  cantidad: ''
}

export default function Ventas() {
  const [data, setData] = useState([])
  const [reload, setReload] = useState(false)

  const [form, setForm] = useState(datosIniciales)
  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    })
  }
  const handleSubmit = (e) => {
    e.preventDefault()

    if (!form.ven_id || !form.cli_id || !form.farmaco_id || !form.cantidad) {
      alert('¡Llena los datos correctamente!')
      return
    }else{
      const venta = async () =>{
        const res = await generarVenta(form)
        alert(res.notificacion)
        setReload(!reload)
        setForm(datosIniciales)
      }
      venta()
    }
  }

  useEffect(function(){
    getVentas().then(ventas => setData(ventas))
  }, [reload])

  return (
    <>
      <h1><i>Ventas</i></h1>
      <div>
          <h3>Crear una venta</h3>
          <form onSubmit={handleSubmit}>

              <label htmlFor="id">Id del vendedor: </label> 
              <input type="number" name="ven_id" id="id"
              onChange={handleChange} value={form.ven_id}/>

              <label htmlFor="cli">Id del cliente: </label>
              <input type="number" name="cli_id" id="cli"
              onChange={handleChange} value={form.cli_id}/>
              <br></br><br></br>

              <label htmlFor="farm">Id del fármaco: </label>
              <input type="number" name="farmaco_id" id="farm"
              onChange={handleChange} value={form.farmaco_id}/>

              <label htmlFor="cantidad">Cantidad: </label>
              <input type="number" name="cantidad" id="cantidad"
              onChange={handleChange} value={form.cantidad}/>

              <input type="submit" value="Crear venta"/>
          </form>
      </div>
      <div>
        <h3>Movimientos de ventas</h3>
        <table id="customers">
        <tr>
          <th>Id Cliente</th>
          <th>Cédula</th>
          <th>Nombres</th>
          <th>Id Fármaco</th>
          <th>Fármaco</th>
          <th>Cantidad</th>
          <th>Fecha</th>
          <th>Id Vendedor</th>
          <th>Vendedor</th>
        </tr>
        {
          data.map((venta) => {
            return(
              <tr key={venta.cli_cedula}>
                <td>{venta.cli_id}</td>
                <td>{venta.cli_cedula}</td>
                <td>{venta.cli_nombres}</td>
                <td>{venta.farmaco_id}</td>
                <td>{venta.farmaco_nombre}</td>
                <td>{venta.detav_cantidad}</td>
                <td>{venta.venta_fecha.substring(0, 10)}</td>
                <td>{venta.ven_id}</td>
                <td>{venta.ven_nombres}</td>
              </tr>
            )
          })
        }
        </table>
      </div>
    </>
  )
}
