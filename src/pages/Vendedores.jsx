import { useState, useEffect } from 'react'
import updateVendedor from '../services/vendedores/updateVendedores'
import deleteVendedor from '../services/vendedores/deleteVendedores'
import readVendedores from '../services/vendedores/readVendedores'

const datosIniciales = {
  ven_id: '',
  suc_id: '',
  estado_id: '',
  horario_id: '',
  ven_nombres: '',
  ven_apellidos: '',
  ven_fechanacimiento: '',
  ven_fechaingreso: '',
  ven_supervisor: ''
}

function Vendedores() {
  const [data, setData] = useState([])
  const [reload, setReload] = useState(false)

  const [form, setForm] = useState(datosIniciales)
  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    })
  }
  const handleSubmit = (e) => {
    e.preventDefault()

    if (!form.ven_id || !form.suc_id || !form.estado_id|| !form.horario_id|| !form.ven_nombres||
      !form.ven_apellidos|| !form.ven_fechanacimiento|| !form.ven_fechaingreso|| !form.ven_supervisor) {
      alert('Datos incompletos')
      return
    }else{
      actualizacion(form)
    }
  }

  const actualizacion = async (body) => {
    const { ven_id, suc_id, estado_id, horario_id, ven_nombres, ven_apellidos, ven_fechanacimiento,
      ven_fechaingreso, ven_supervisor } = body
    await updateVendedor(ven_id, {ven_id, suc_id, estado_id, horario_id, ven_nombres, ven_apellidos, ven_fechanacimiento,
      ven_fechaingreso, ven_supervisor})
    setReload(!reload)
    setForm(datosIniciales)
  }

  useEffect(function(){
    readVendedores().then(v => setData(v))
  }, [reload])

  return (
    <>
      <div>
        <h1><i>Vendedores</i></h1>
        <div>
          <h3>Crea o actualiza un vendedor</h3>
          <form onSubmit={handleSubmit}>
            <div>
              <label htmlFor="id">Id del vendedor: </label> 
              <input type="number" name="ven_id" id="id"
              onChange={handleChange} value={form.ven_id} className='right'/>

              <label htmlFor="id2">Id sucursal: </label> 
              <input type="number" name="suc_id" id="id2"
              onChange={handleChange} value={form.suc_id} className='right'/>

              <label htmlFor="id3">Id estado: </label> 
              <input type="number" name="estado_id" id="id3"
              onChange={handleChange} value={form.estado_id} className='right'/> <br /> <br />

              <label htmlFor="id4">Id horario: </label> 
              <input type="number" name="horario_id" id="id4"
              onChange={handleChange} value={form.horario_id} className='right'/>

              <label htmlFor="cantidad">Nombres: </label>
              <input type="text" name="ven_nombres" id="cantidad"
              onChange={handleChange} value={form.ven_nombres} className='right'/>

              <label htmlFor="cantidad2">Apellidos: </label>
              <input type="text" name="ven_apellidos" id="cantidad2"
              onChange={handleChange} value={form.ven_apellidos} className='right'/> <br /> <br />

              <label htmlFor="cantidad3">Fecha nacimiento: </label>
              <input type="date" name="ven_fechanacimiento" id="cantidad3"
              onChange={handleChange} value={form.ven_fechanacimiento} className='right'/>

              <label htmlFor="cantidad4">Fecha ingreso: </label>
              <input type="date" name="ven_fechaingreso" id="cantidad4"
              onChange={handleChange} value={form.ven_fechaingreso} className='right'/>

              <label htmlFor="id5">Id supervisor: </label> 
              <input type="number" name="ven_supervisor" id="id5"
              onChange={handleChange} value={form.ven_supervisor} className='right'/> <br /> <br />

              <input type="submit" value="Enviar"/>
            </div>
          </form>
        </div>

        <div>
          <h3>Listado de Vendedores</h3>
          <table id="customers">
          <tr>
            <th>Id vendedor</th>
            <th>Id sucursal</th>
            <th>Id estado</th>
            <th>Id horario</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Fecha de nacimiento</th>
            <th>Fecha de ingreso</th>
            <th>Id supervisor</th>
            <th colSpan={2}>Acciones</th>
          </tr>
          {
            data.map((v) => {
              return(
                <tr key={v.ven_id}>
                  <td>{v.ven_id}</td>
                  <td>{v.suc_id}</td>
                  <td>{v.estado_id}</td>
                  <td>{v.horario_id}</td>
                  <td>{v.ven_nombres}</td>
                  <td>{v.ven_apellidos}</td>
                  <td>{v.ven_fechanacimiento.substring(0, 10)}</td>
                  <td>{v.ven_fechaingreso.substring(0, 10)}</td>
                  <td>{v.ven_supervisor}</td>
                  <td>
                      <button
                        onClick={() => {
                          v.ven_fechanacimiento = v.ven_fechanacimiento.slice(0, 10)
                          v.ven_fechaingreso = v.ven_fechaingreso.slice(0, 10)
                          setForm(v)
                        }}
                      >Editar</button>
                    </td>
                    <td>
                      <button
                        onClick={ async () => {
                          await deleteVendedor(v.ven_id)
                          setReload(!reload)
                        }}
                      >Eliminar</button>
                    </td>
                </tr>
              )
            })
          }
          </table>
        </div>
        
      </div>
    </>
  )
}

export default Vendedores
