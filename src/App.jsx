import { Route } from 'wouter'
import Nav from '../src/components/Nav'
import Ventas from '../src/pages/Ventas'
import Compras from '../src/pages/Compras'
import Vendedores from './pages/Vendedores'
import Clientes from './pages/Clientes'

function App() {

  return (
    <div>
      <Nav/>
      <div className='App'>
        <Route path='/' component={Ventas}/>
        <Route path='/compras' component={Compras}/>
        <Route path='/vendedores' component={Vendedores}/>
        <Route path='/clientes' component={Clientes}/>
      </div>
    </div>
  )
}

export default App
