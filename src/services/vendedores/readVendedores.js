import {API} from '../index'

export default function readVendedores(){

  const url = `${API}/vendedor`

  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(request => request.json())
    .then(response => response)
}