import {API} from '../index'

export default function deleteVendedores(id){
  const url = `${API}/vendedor/${id}`

  return fetch(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(request => request.json())
    .then(response => response)
}