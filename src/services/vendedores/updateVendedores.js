import {API} from '../index'

export default function updateVendedores(id, body){
  const url = `${API}/vendedor/${id}`

  return fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body)
  })
    .then(request => request.json())
    .then(response => response)
}