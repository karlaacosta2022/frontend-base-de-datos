import {API} from '../index'

export default function deleteClientes(id){
  const url = `${API}/cliente/${id}`

  return fetch(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(request => request.json())
    .then(response => response)
}