import {API} from '../index'

export default function readClientes(){

  const url = `${API}/cliente`

  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(request => request.json())
    .then(response => response)
}