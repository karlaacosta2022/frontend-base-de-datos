import {API} from '.'

export default function getVentas(){

  const url = `${API}/venta`

  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(request => request.json())
    .then(response => response)
}