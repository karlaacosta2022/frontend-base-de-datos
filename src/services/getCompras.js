import {API} from '.'

export default function getCompras(){

  const url = `${API}/compra`

  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(request => request.json())
    .then(response => response)
}