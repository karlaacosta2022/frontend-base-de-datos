import { Link } from "wouter"
import './Nav.css'

function Nav() {
  return (
    <>
    <header className="header">
      <nav>
        <ul className="ul">
          <li className="li">
            <Link href="/" className="active">Ventas</Link>
          </li>
          <li className="li">
            <Link href="/compras" className="active">Compras</Link>
          </li>
          <li className="li">
            <Link href="/clientes" className="active">Clientes</Link>
          </li>
          <li className="li">
            <Link href="/vendedores" className="active">Vendedores</Link>
          </li>
        </ul>
      </nav>
    </header>
    </>
  )
}

export default Nav